/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.sql.Date;

/**
 *
 * @author ASUS
 */
public class Candidature {
    private int id,id_projet,id_freelancer;
    private Date date;
    private String titre,description;

    public Candidature(String titre) {
        this.titre = titre;
    }

    public Candidature(Date date, String titre) {
        this.date = date;
        this.titre = titre;
    }

    public Candidature(int aInt, String string, int aInt0, int aInt1, Date date) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_projet() {
        return id_projet;
    }

    public void setId_projet(int id_projet) {
        this.id_projet = id_projet;
    }

    public int getId_freelancer() {
        return id_freelancer;
    }

    public void setId_freelancer(int id_freelancer) {
        this.id_freelancer = id_freelancer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Candidature(int id, String titre, String description,int id_projet, int id_freelancer, Date date ) {
        this.id = id;
        this.id_projet = id_projet;
        this.id_freelancer = id_freelancer;
        this.date = date;
        this.titre = titre;
        this.description = description;
    }

    public Candidature() {
    }

   

   
        
    

 
    
}
