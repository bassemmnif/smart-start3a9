/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author ASUS
 */
public class Projet {
     private int id,id_entreprise,id_freelancer ; 
    private String titre,etat,description ;
    private Date date_debut,date_fin ; 
    private float prix ;  

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_entreprise() {
        return id_entreprise;
    }

    public void setId_entreprise(int id_entreprise) {
        this.id_entreprise = id_entreprise;
    }

    public int getId_freelancer() {
        return id_freelancer;
    }

    public void setId_freelancer(int id_freelancer) {
        this.id_freelancer = id_freelancer;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Date getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(Date date_debut) {
        this.date_debut = date_debut;
    }

    public Date getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(Date date_fin) {
        this.date_fin = date_fin;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Projet() {
    }

    public Projet(int id,  String titre, String description, Date date_debut, Date date_fin, float prix,int id_entreprise, int id_freelancer) {
        this.id = id;
        this.titre = titre;
        this.description = description;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.prix = prix;
        this.id_entreprise = id_entreprise;
        this.id_freelancer = id_freelancer;
    }

    
    
    public Projet(int id, int id_entreprise, int id_freelancer, String titre, String etat, String description, Date date_debut, Date date_fin, float prix) {
        this.id = id;
        this.id_entreprise = id_entreprise;
        this.id_freelancer = id_freelancer;
        this.titre = titre;
        this.etat = etat;
        this.description = description;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.prix = prix;
    }

    public Projet(int id, String titre, String description, Date date_debut, Date date_fin, float prix) {
        this.id = id;
        this.titre = titre;
        this.description = description;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.prix = prix;
    }

    
    
    
}
