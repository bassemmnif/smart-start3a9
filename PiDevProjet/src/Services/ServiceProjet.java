/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entities.Candidature;
import java.sql.*;
import Entities.Projet;
import Entities.Publication;
import Utils.ConnexionBD;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class ServiceProjet {

    Connection C = ConnexionBD.getInstance().getConn();

    public void afficherProjet() {
        try {
            Statement st = C.createStatement();
            String req = "select * from projet";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                System.out.println("projet" + rs.getInt(1) + " " + "titre " + " " + rs.getString(2) + "etat: " + " " + rs.getString(3) + "date_debut" + rs.getDate(4) + "date_fin" + rs.getDate(5) + "prix" + rs.getFloat(6));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceProjet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Projet> afiiche() throws SQLException {
        ArrayList<Projet> myList = new ArrayList();
        Statement st = C.createStatement();
        String req = "select * from projet";
        ResultSet rs = st.executeQuery(req);
        while (rs.next()) {
            Projet r;

            r = new Projet(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(5), rs.getDate(6), rs.getFloat(7));

            myList.add(r);

        }
        return myList;
    }

    public void AjouterCandidature(int id) {
        try {
            java.util.Date utilDate = new java.util.Date();

            java.sql.Date date = new java.sql.Date(utilDate.getTime());
            Statement st1 = C.createStatement();
            String req = "insert into candidature(id_projet,id_freelancer,date) values('" +id+ "','" + 3 + "','" + date + "')";

            st1.executeUpdate(req);
        } catch (SQLException ex) {
            Logger.getLogger(ServiceProjet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String Titre(int id) {
        String ch = "";
        try {

            Statement st1 = C.createStatement();
            String req = "select * from projet";
            ResultSet rs = st1.executeQuery(req);
            while (rs.next()) {
                ch = rs.getString(2);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ServiceProjet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ch;
    }

    public ArrayList<Projet> afiicheMesProjet() throws SQLException {
        ArrayList<Projet> myList = new ArrayList();
        Statement st = C.createStatement();
        String req = "select * from projet where id_freelancer=3";
        ResultSet rs = st.executeQuery(req);
        while (rs.next()) {
            Projet r;

            r = new Projet(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(5), rs.getDate(6), rs.getFloat(7));

            myList.add(r);

        }
        return myList;
    }

    public ArrayList<Publication> rechParPrix(float min, float max) throws SQLException {
        ArrayList<Publication> myList = new ArrayList();
        Statement st = C.createStatement();
        String req = "select * from publication where prix BETWEEN '" + min + "'AND'" + max + "'";
        ResultSet rs = st.executeQuery(req);
        while (rs.next()) {
           Publication p;

            p = new Publication(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4), rs.getDate(5), rs.getFloat(6));

            myList.add(p);

        }
        return myList;

    }

    public ArrayList<Publication> rechPardes(String des) throws SQLException {
        ArrayList<Publication> myList = new ArrayList();
        Statement st = C.createStatement();
        String req = "select * from publication where description like'%" + des + "%'";
        ResultSet rs = st.executeQuery(req);
        while (rs.next()) {
            Publication p;

            p = new Publication(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4), rs.getDate(5), rs.getFloat(6));

            myList.add(p);

        }
        return myList;

    }

    public ArrayList<Publication> rech(float min, float max, String des) throws SQLException {
        ArrayList<Publication> myList = new ArrayList();
        Statement st = C.createStatement();
        String req = "select * from publication where (description like'%" + des + "%') AND prix BETWEEN '" + min + "'AND'" + max + "'";
        ResultSet rs = st.executeQuery(req);
        while (rs.next()) {
            Publication p;

            p = new Publication(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4), rs.getDate(5), rs.getFloat(6));

            myList.add(p);

        }
        return myList;

    }

    public ArrayList<Publication> afiichePublication() throws SQLException {
        ArrayList<Publication> myList = new ArrayList();
        Statement st = C.createStatement();
        String req = "select * from publication";
        ResultSet rs = st.executeQuery(req);
        while (rs.next()) {
            Publication p;

            p = new Publication(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4), rs.getDate(5), rs.getFloat(6));

            myList.add(p);

        }
        return myList;
    }

    
    
     public ArrayList<Projet> afficheAdmin() throws SQLException {
        ArrayList<Projet> myList = new ArrayList();
        Statement st = C.createStatement();
        String req = "select * from projet";
        ResultSet rs = st.executeQuery(req);
        while (rs.next()) {
            Projet r;

            r = new Projet(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getDate(5),rs.getDate(6),rs.getFloat(7),rs.getInt(8),rs.getInt(9));

            myList.add(r);

        }
        return myList;
    }
}
