/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Projet;
import Services.ServiceProjet;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class MesProjetsController implements Initializable {

   @FXML
    private TableColumn<Projet, Integer> id;
    @FXML
    private TableColumn<Projet, String> titre;
   
    @FXML
    private TableColumn<Projet,Date> date_debut;
    @FXML
    private TableColumn<Projet,Date> date_fin;
    @FXML
    private TableColumn<Projet,Float> prix;
    @FXML
    private TableView<Projet> TabMesProjets;
    @FXML
    private Button BtnRtr;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
     try {
            ServiceProjet servP = new ServiceProjet();
            ArrayList<Projet> r = servP.afiicheMesProjet() ;
            ObservableList<Projet>  list = FXCollections.observableArrayList();
            for (int i = 0; i < r.size(); i++)
            {
                
                Projet t = new Projet(); 
                t.setId(r.get(i).getId());
                t.setTitre(r.get(i).getTitre());
              
                t.setDate_debut(r.get(i).getDate_debut());
                t.setDate_fin(r.get(i).getDate_fin());
                t.setPrix(r.get(i).getPrix());
                list.add(t);
            }
            
            
            
            id.setCellValueFactory(new PropertyValueFactory<Projet,Integer>("id"));
            titre.setCellValueFactory(new PropertyValueFactory<Projet,String>("titre"));
           
           date_debut.setCellValueFactory(new PropertyValueFactory<Projet,Date>("date_debut"));
            date_fin.setCellValueFactory(new PropertyValueFactory<Projet,Date>("date_fin"));
            prix.setCellValueFactory(new PropertyValueFactory<Projet,Float>("prix"));
            TabMesProjets.setItems(list);
        } catch (SQLException ex) {
            Logger.getLogger(MesProjetsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @FXML
    private void Retour(ActionEvent event) throws IOException {
        FXMLLoader load1 = new FXMLLoader(getClass().getResource("/Views/InterfaceProjet.fxml")); 
           Parent root = load1.load();
           InterfaceProjetController irc = load1.getController();
          
           BtnRtr.getScene().setRoot(root);
    }
    
}
