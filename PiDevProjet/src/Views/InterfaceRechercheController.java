/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Projet;
import Entities.Publication;
import Services.ServiceProjet;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class InterfaceRechercheController implements Initializable {

    @FXML
    private JFXComboBox<String> Combo;
    @FXML
    private JFXTextField txtMin;
    @FXML
    private JFXTextField txtMax;
    @FXML
    private JFXTextField txtDescription;
    @FXML
    private TableView<Publication> TabRecherche;
    
    @FXML
    private TableColumn<Publication, Integer> id;
    @FXML
    private TableColumn<Publication, String> titre;
    @FXML
    private TableColumn<Publication, String> description;
    @FXML
    private TableColumn<Publication,Date> date_debut;
    @FXML
    private TableColumn<Publication,Date> date_fin;
    @FXML
    private TableColumn<Publication,Float> prix;
    @FXML
    private JFXButton BtnRech;
    @FXML
    private Button BtnRtr;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<String>  l = FXCollections.observableArrayList("par Prix","Par Description","Par Description&prix");
        Combo.setItems(l); // TODO
    }    

    @FXML
    private void ListRecherche(ActionEvent event) {
       
    }

    @FXML
    private void RechProjet(ActionEvent event) {
        String value = (String)Combo.getSelectionModel().getSelectedItem(); 
        if(value=="par Prix")
        {
            try {
                ServiceProjet servP = new ServiceProjet();
                ArrayList<Publication> r = servP.rechParPrix((Float.parseFloat(txtMin.getText())),(Float.parseFloat(txtMax.getText())));
                ObservableList<Publication>  list = FXCollections.observableArrayList();
                for (int i = 0; i < r.size(); i++)
                {
                    
                    Publication t = new Publication();
                    t.setId(r.get(i).getId());
                    t.setTitre(r.get(i).getTitre());
                    t.setDescription(r.get(i).getDescription());
                    t.setDate_debut(r.get(i).getDate_debut());
                    t.setDate_fin(r.get(i).getDate_fin());
                    t.setPrix(r.get(i).getPrix());
                    list.add(t);
                }
                
                id.setCellValueFactory(new PropertyValueFactory<Publication,Integer>("id"));
                titre.setCellValueFactory(new PropertyValueFactory<Publication,String>("titre"));
                description.setCellValueFactory(new PropertyValueFactory<Publication,String>("description"));
                date_debut.setCellValueFactory(new PropertyValueFactory<Publication,Date>("date_debut"));
                date_fin.setCellValueFactory(new PropertyValueFactory<Publication,Date>("date_fin"));
                prix.setCellValueFactory(new PropertyValueFactory<Publication,Float>("prix"));
                TabRecherche.setItems(list);
            } catch (SQLException ex) {
                Logger.getLogger(InterfaceRechercheController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
         if(value=="Par Description")
        {
            try {
                ServiceProjet servP = new ServiceProjet();
                ArrayList<Publication> r = servP.rechPardes(txtDescription.getText());
                ObservableList<Publication>  list = FXCollections.observableArrayList();
                for (int i = 0; i < r.size(); i++)
                {
                    
                    Publication t = new Publication();
                    t.setId(r.get(i).getId());
                    t.setTitre(r.get(i).getTitre());
                    t.setDescription(r.get(i).getDescription());
                    t.setDate_debut(r.get(i).getDate_debut());
                    t.setDate_fin(r.get(i).getDate_fin());
                    t.setPrix(r.get(i).getPrix());
                    list.add(t);
                }
                
                id.setCellValueFactory(new PropertyValueFactory<Publication,Integer>("id"));
                titre.setCellValueFactory(new PropertyValueFactory<Publication,String>("titre"));
                description.setCellValueFactory(new PropertyValueFactory<Publication,String>("description"));
                date_debut.setCellValueFactory(new PropertyValueFactory<Publication,Date>("date_debut"));
                date_fin.setCellValueFactory(new PropertyValueFactory<Publication,Date>("date_fin"));
                prix.setCellValueFactory(new PropertyValueFactory<Publication,Float>("prix"));
                TabRecherche.setItems(list);
            } catch (SQLException ex) {
                Logger.getLogger(InterfaceRechercheController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
          if(value=="Par Description&prix")
        {
            try {
                ServiceProjet servP = new ServiceProjet();
                ArrayList<Publication> r = servP.rech((Float.parseFloat(txtMin.getText())),(Float.parseFloat(txtMax.getText())), txtDescription.getText());
                ObservableList<Publication>  list = FXCollections.observableArrayList();
                for (int i = 0; i < r.size(); i++)
                {
                    
                    Publication t = new Publication();
                    t.setId(r.get(i).getId());
                    t.setTitre(r.get(i).getTitre());
                    t.setDescription(r.get(i).getDescription());
                    t.setDate_debut(r.get(i).getDate_debut());
                    t.setDate_fin(r.get(i).getDate_fin());
                    t.setPrix(r.get(i).getPrix());
                    list.add(t);
                }
                 id.setCellValueFactory(new PropertyValueFactory<Publication,Integer>("id"));
                titre.setCellValueFactory(new PropertyValueFactory<Publication,String>("titre"));
                description.setCellValueFactory(new PropertyValueFactory<Publication,String>("description"));
                date_debut.setCellValueFactory(new PropertyValueFactory<Publication,Date>("date_debut"));
                date_fin.setCellValueFactory(new PropertyValueFactory<Publication,Date>("date_fin"));
                prix.setCellValueFactory(new PropertyValueFactory<Publication,Float>("prix"));
                TabRecherche.setItems(list);
                
            } catch (SQLException ex) {
                Logger.getLogger(InterfaceRechercheController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
    }

    @FXML
    private void Retour(ActionEvent event) throws IOException {
         FXMLLoader load1 = new FXMLLoader(getClass().getResource("/Views/InterfaceProjet.fxml")); 
           Parent root = load1.load();
           InterfaceProjetController irc = load1.getController();
          
           BtnRtr.getScene().setRoot(root);
    }
    
}
