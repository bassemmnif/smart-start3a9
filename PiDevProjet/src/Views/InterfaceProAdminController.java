/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Projet;
import Services.ServiceProjet;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class InterfaceProAdminController implements Initializable {

    @FXML
    private TableView<Projet> TabPub;

    @FXML
    private TableColumn<Projet, Integer> id;
    @FXML
    private TableColumn<Projet, String> titre;

    @FXML
    private TableColumn<Projet, Date> date_debut;
    @FXML
    private TableColumn<Projet, Date> date_fin;
    @FXML
    private TableColumn<Projet, Float> prix;
    @FXML
    private TableColumn<Projet, Integer> id_entreprise;
    @FXML
    private TableColumn<Projet, Integer> id_freelance;
    @FXML
    private TableColumn<Projet,String> description;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ServiceProjet servP = new ServiceProjet();
            ArrayList<Projet> r = servP.afficheAdmin();
            ObservableList<Projet> list = FXCollections.observableArrayList();
            for (int i = 0; i < r.size(); i++) {

                Projet t = new Projet();
                t.setId(r.get(i).getId());
                t.setTitre(r.get(i).getTitre());
                t.setDescription(r.get(i).getDescription());

                t.setDate_debut(r.get(i).getDate_debut());
                t.setDate_fin(r.get(i).getDate_fin());
                t.setPrix(r.get(i).getPrix());
                t.setId_entreprise(r.get(i).getId_entreprise());
                t.setId_freelancer(r.get(i).getId_freelancer());
                list.add(t);
            }

            id.setCellValueFactory(new PropertyValueFactory<Projet, Integer>("id"));
            titre.setCellValueFactory(new PropertyValueFactory<Projet, String>("titre"));
            description.setCellValueFactory(new PropertyValueFactory<Projet, String>("description"));

            date_debut.setCellValueFactory(new PropertyValueFactory<Projet, Date>("date_debut"));
            date_fin.setCellValueFactory(new PropertyValueFactory<Projet, Date>("date_fin"));
            prix.setCellValueFactory(new PropertyValueFactory<Projet, Float>("prix"));
            id_entreprise.setCellValueFactory(new PropertyValueFactory<Projet, Integer>("id_entreprise"));
            id_freelance.setCellValueFactory(new PropertyValueFactory<Projet, Integer>("id_freelancer"));
            TabPub.setItems(list);
        } catch (SQLException ex) {
            Logger.getLogger(MesProjetsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
