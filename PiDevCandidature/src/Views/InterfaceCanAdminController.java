/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Candidature;
import Services.ServiceCandidature;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class InterfaceCanAdminController implements Initializable {

    
    @FXML
    private TableView<Candidature> TabCand;
   
    @FXML
    private TableColumn<Candidature, String> titre;
    
   
    @FXML
    private TableColumn<Candidature, Integer> id_freelancer;
    @FXML
    private TableColumn<Candidature, Date> date;
    @FXML
    private JFXTextField txtIdsup;
    @FXML
    private TableColumn<Candidature, Integer> id;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      try {
            ServiceCandidature srv = new ServiceCandidature();
            ArrayList<Candidature> r = srv.afficheTousCan();
            ObservableList<Candidature>  list = FXCollections.observableArrayList();
            
            for (int i = 0; i < r.size(); i++)
            {
                String a = srv.AfficherTitre(r.get(i).getId_projet());
                Candidature t = new Candidature(); 
                
                t.setId(r.get(i).getId());
                t.setTitre(a); 
               t.setId_projet(r.get(i).getId_projet());
                t.setId_freelancer(r.get(i).getId_freelancer());
                t.setDate(r.get(i).getDate());
                list.add(t);
            }
            
            
            
            id.setCellValueFactory(new PropertyValueFactory<Candidature,Integer>("id"));
            titre.setCellValueFactory(new PropertyValueFactory<Candidature,String>("titre"));
          
            id_freelancer.setCellValueFactory(new PropertyValueFactory<Candidature,Integer>("id_freelancer"));
            date.setCellValueFactory(new PropertyValueFactory<Candidature,Date>("date"));
            TabCand.setItems(list);
        } catch (SQLException ex) {
            Logger.getLogger(InterfaceCandidatureController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }    
    
}
