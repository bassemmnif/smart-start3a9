/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.sql.Date;

/**
 *
 * @author ASUS
 */
public class Candidature {
    private int id,id_projet,id_freelancer;
    private Date date;
    private String titre;

    public int getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_projet() {
        return id_projet;
    }

    public void setId_projet(int id_projet) {
        this.id_projet = id_projet;
    }

    public int getId_freelancer() {
        return id_freelancer;
    }

    public void setId_freelancer(int id_freelancer) {
        this.id_freelancer = id_freelancer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    

    public Candidature(int id, String titre,int id_projet, int id_freelancer, Date date ) {
        this.id = id;
        this.id_projet = id_projet;
        this.id_freelancer = id_freelancer;
        this.date = date;
     
        
    }

    public Candidature(int id, int id_projet, int id_freelancer, Date date) {
        this.id = id;
        this.id_projet = id_projet;
        this.id_freelancer = id_freelancer;
        this.date = date;
    }

    
    public Candidature() {
    }

   

   
        
    

 
    
}
