/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.sql.Date;

/**
 *
 * @author ASUS
 */
public class Reclamation {

    private String a;

    public Reclamation(int id, String message, String objet, Date date) {
        this.id = id;
        this.message = message;
        this.objet = objet;
        this.date = date;
    }

    public Reclamation(int id_user) {
        this.id_user = id_user;
    }

    private int id, id_user;
    private String message, objet;
    private Date date;

    public Reclamation(int id, int id_user, String message, String objet) {
        this.id = id;
        this.id_user = id_user;
        this.message = message;
        this.objet = objet;
    }

    public Reclamation() {
    }

    public Reclamation(int id, int id_user, String message, String objet, Date date) {
        this.id = id;
        this.id_user = id_user;
        this.message = message;
        this.objet = objet;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Reclamation(int id_user, String message, String objet) {
        this.id_user = id_user;
        this.message = message;
        this.objet = objet;
    }

    public int getId() {
        return id;
    }

    public int getId_user() {
        return id_user;
    }

    public String getMessage() {
        return message;
    }

    public String getObjet() {
        return objet;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public void setEmail(String a) {
        this.a = a;

    }

}
