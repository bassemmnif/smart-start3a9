/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Reclamation;
import Services.ServiceReclamation;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import mailapp.SendMail;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class MailAdminController implements Initializable {

    @FXML
    private TextField to;
    @FXML
    private TextField titre;
    @FXML
    private TextField message;
    @FXML
    private Button btnEnvoi;
    @FXML
    private Button BtnRtr;

    public void setLabTo(String t){
        to.setText(t);
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void envoyer(ActionEvent event) throws SQLException {
         
        SendMail.send(to.getText(), titre.getText(), message.getText(), "pifreedev19@gmail.com", "123456789aA.");
         System.exit(0);
    }

    @FXML
    private void Retour(ActionEvent event) throws IOException {
         FXMLLoader load1 = new FXMLLoader(getClass().getResource("/Views/InterfaceRecAdmin.fxml")); 
           Parent root = load1.load();
           InterfaceRecAdminController irc = load1.getController();
          
           BtnRtr.getScene().setRoot(root);
        
    }
    
}
