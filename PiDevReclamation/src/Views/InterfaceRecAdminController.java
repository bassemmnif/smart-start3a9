/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;


import Entities.Reclamation;
import Services.ServiceReclamation;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mailapp.SendMail;
import pidevreclamation.PiDevReclamation;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class InterfaceRecAdminController implements Initializable {

    @FXML
    private TableView<Reclamation> Tab;
    @FXML
    private TableColumn<Reclamation, Integer> id;
    
    @FXML
    private TableColumn<Reclamation, String> Msg;
    @FXML
    private TableColumn<Reclamation, String> Obj;
    @FXML
    private TableColumn<Reclamation, Date> Date;
    @FXML
    private JFXTextField txtIdsup;
    @FXML
    private JFXButton BtnSupp;
    @FXML
    private JFXButton BtnRep;
    @FXML
    private JFXTextField txtRep;
   
    @FXML
    private TableColumn<Reclamation, Integer> id_user;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ServiceReclamation srv = new ServiceReclamation();
            ArrayList<Reclamation> r = srv.afficheAdmin();
            ObservableList<Reclamation> list = FXCollections.observableArrayList();
            for (int i = 0; i < r.size(); i++) {
                 
                Reclamation t = new Reclamation(); 
                  ;
              
                t.setId(r.get(i).getId());
                 t.setId_user(r.get(i).getId_user());
          
                t.setMessage(r.get(i).getMessage());
                t.setObjet(r.get(i).getObjet());
                t.setDate(r.get(i).getDate());
                list.add(t);
            }

            id.setCellValueFactory(new PropertyValueFactory<Reclamation, Integer>("id"));
            id_user.setCellValueFactory(new PropertyValueFactory<Reclamation, Integer>("id_user"));
            
            Msg.setCellValueFactory(new PropertyValueFactory<Reclamation, String>("Message"));
            Obj.setCellValueFactory(new PropertyValueFactory<Reclamation, String>("Objet"));
            Date.setCellValueFactory(new PropertyValueFactory<Reclamation, Date>("Date"));
            Tab.setItems(list);
        } catch (SQLException ex) {
            Logger.getLogger(InterfaceReclamationController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void SupprimerRec(ActionEvent event) {
        ServiceReclamation serv = new ServiceReclamation();
        serv.SupprimerReclamation(Integer.parseInt(txtIdsup.getText()));
        try {
            ServiceReclamation srv = new ServiceReclamation();
            ArrayList<Reclamation> r = srv.afficheAdmin();
            ObservableList<Reclamation> list = FXCollections.observableArrayList();
            for (int i = 0; i < r.size(); i++) {
               
                Reclamation t = new Reclamation(); 
              
                t.setId(r.get(i).getId());
                t.setId_user(r.get(i).getId_user());
                   
                t.setMessage(r.get(i).getMessage());
                t.setObjet(r.get(i).getObjet());
                t.setDate(r.get(i).getDate());
                list.add(t);
            }

            id.setCellValueFactory(new PropertyValueFactory<Reclamation, Integer>("id"));
             id_user.setCellValueFactory(new PropertyValueFactory<Reclamation, Integer>("id_user"));
            
            Msg.setCellValueFactory(new PropertyValueFactory<Reclamation, String>("Message"));
            Obj.setCellValueFactory(new PropertyValueFactory<Reclamation, String>("Objet"));
            Date.setCellValueFactory(new PropertyValueFactory<Reclamation, Date>("Date"));
            Tab.setItems(list);
        } catch (SQLException ex) {
            Logger.getLogger(InterfaceReclamationController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML

    private void Repondre(ActionEvent event) throws IOException, SQLException {
         FXMLLoader load1 = new FXMLLoader(getClass().getResource("/Views/MailAdmin.fxml")); 
           Parent root = load1.load();
           MailAdminController irc = load1.getController();
           
   /* ServiceReclamation srv = new ServiceReclamation();
         
           String em=""; 
       em = srv.AfficherEmail(srv.Getttt());
               
            System.err.println(em);
            irc.setLabTo(em);*/
      
      
       /*String t="" ;
          ServiceReclamation srv = new ServiceReclamation();
      t = srv.AfficherEmail(1);
                  irc.setLabTo(t);*/

      
           BtnRep.getScene().setRoot(root);
       
    }

}

