/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Reclamation;
import Services.ServiceReclamation;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class AjouterReclamationController implements Initializable {

    @FXML
    private TextField txtUsername;
    @FXML
    private TextField TxtObjet;
    @FXML
    private TextField TxtMessage;
    @FXML
    private Button BtnEnvoyer;
    @FXML
    private Button BtnRtr;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    


    @FXML
    private void Envoyer(ActionEvent event) {
       ServiceReclamation srv = new ServiceReclamation();
        int f = srv.AfficherNom(txtUsername.getText());  
    Reclamation r = new Reclamation(f, TxtMessage.getText(), TxtObjet.getText());
        srv.AjouterReclamation(r);
          txtUsername.setText("");
          TxtMessage.setText("");
          TxtObjet.setText("");
    }

    @FXML
    private void exit(MouseEvent event) {
        System.exit(0);
    }

    @FXML
    private void Retour(ActionEvent event) throws IOException {
          FXMLLoader load1 = new FXMLLoader(getClass().getResource("/Views/InterfaceReclamation.fxml")); 
           Parent root = load1.load();
           InterfaceReclamationController irc = load1.getController();
          
           BtnEnvoyer.getScene().setRoot(root);
            
    }
    
}
