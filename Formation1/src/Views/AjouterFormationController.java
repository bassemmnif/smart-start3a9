/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Formation;
import Service.FormationService;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class AjouterFormationController implements Initializable {

    @FXML
    private JFXButton ajouter;
    @FXML
    private JFXTextField url;
    @FXML
    private JFXTextField nom;
    @FXML
    private JFXButton r;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void ajouterFormation(ActionEvent event) {
        Formation f = new Formation(nom.getText(), url.getText());
        FormationService srv = new FormationService();
        srv.AjouterFormation(f);
        url.setText("");
        nom.setText("");
    }

    @FXML
    private void retour(ActionEvent event) throws IOException {
          FXMLLoader load1 = new FXMLLoader(getClass().getResource("/Views/Formation.fxml")); 
           Parent root = load1.load(); 
           FormationController irc = load1.getController();
           
           r.getScene().setRoot(root);
    }
    
}
