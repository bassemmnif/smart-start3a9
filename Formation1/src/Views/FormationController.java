/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Formation;
import Service.FormationService;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class FormationController implements Initializable {

    @FXML
    private JFXButton btn;
    @FXML
    private JFXTextField txtid;
    @FXML
    private JFXButton btnaj1;
    @FXML
    private TableView<Formation> table;
    @FXML
    private TableColumn<Formation, Integer> id;
    @FXML
    private TableColumn<Formation, String> nom;
    @FXML
    private TableColumn<Formation, Date> date;
    @FXML
    private TableColumn<Formation, String> url1;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
          
        try {
            FormationService srv = new FormationService();
            ArrayList<Formation> r = srv.afiiche() ;
            ObservableList<Formation>  list = FXCollections.observableArrayList();
            for (int i = 0; i < r.size(); i++)
            {
                Formation t = new Formation();
                
                t.setId(r.get(i).getId());
                t.setNom(r.get(i).getNom());
                t.setUrl(r.get(i).getUrl());
                t.setDate(r.get(i).getDate());
                
                list.add(t);
            }
             id.setCellValueFactory(new PropertyValueFactory<Formation,Integer>("id"));
             nom.setCellValueFactory(new PropertyValueFactory<Formation,String>("nom"));
            
             date.setCellValueFactory(new PropertyValueFactory<Formation,Date>("date"));
              url1.setCellValueFactory(new PropertyValueFactory<Formation,String>("url")); 
              table.setItems(list);
        } catch (SQLException ex) {
            Logger.getLogger(FormationController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @FXML
    private void supid(ActionEvent event) {
        FormationService srv = new FormationService(); 
     srv.suppr(Integer.parseInt(txtid.getText()));
       try {
            ArrayList<Formation> r = srv.afiiche() ;
            ObservableList<Formation>  list = FXCollections.observableArrayList();
            for (int i = 0; i < r.size(); i++)
            {
                Formation t = new Formation();
                
                t.setId(r.get(i).getId());
                t.setNom(r.get(i).getNom());
                t.setUrl(r.get(i).getUrl());
                t.setDate(r.get(i).getDate());
                
                list.add(t);
            }
             id.setCellValueFactory(new PropertyValueFactory<Formation,Integer>("id"));
             nom.setCellValueFactory(new PropertyValueFactory<Formation,String>("nom"));
            
             date.setCellValueFactory(new PropertyValueFactory<Formation,Date>("date"));
              url1.setCellValueFactory(new PropertyValueFactory<Formation,String>("url")); 
              table.setItems(list);
        } catch (SQLException ex) {
            Logger.getLogger(FormationController.class.getName()).log(Level.SEVERE, null, ex);
        }
     txtid.setText("");
    }
		Stage primaryStage = new Stage();

    @FXML
    private void ajouterT(ActionEvent event) throws IOException {
        FXMLLoader load1 = new FXMLLoader(getClass().getResource("/Views/AjouterFormation.fxml")); 
           Parent root = load1.load(); 
           AjouterFormationController irc = load1.getController();
         
           btn.getScene().setRoot(root);
    }}
    

