/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Formation;
import Service.FormationService;
import com.jfoenix.controls.JFXComboBox;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class ConsulterFormationController implements Initializable {

    @FXML
    private JFXComboBox<String> list;
    @FXML
    private WebView webView;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            FormationService srv = new FormationService();
            ArrayList<String> r = srv.listf() ;
          
            ObservableList<String>  l = FXCollections.observableArrayList();
            for (int i = 0; i < r.size(); i++)
            {
                l.add(r.get(i));
            }
            list.setItems(l);
        } catch (SQLException ex) {
            Logger.getLogger(ConsulterFormationController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
   
    @FXML
    private void lire(ActionEvent event){
        
        String value = (String)list.getSelectionModel().getSelectedItem();
         FormationService srv = new FormationService(); 
            String url = srv.idf(value);
            System.err.println(url);
             
    webView.getEngine().load(url);
   
    
}}
