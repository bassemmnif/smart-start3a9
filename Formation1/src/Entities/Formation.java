package Entities;



import java.sql.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Asus
 */
public class Formation {
  int id;
  String nom,url;
 
  Date date ;  

    public Formation() {
    }

    public Formation(String nom, String url, Date date) {
        this.nom = nom;
        this.url = url;
        this.date = date;
    }

    public Formation(int id, String nom, String url, Date date) {
        this.id = id;
        this.nom = nom;
        this.url = url;
        this.date = date;
    }

    public Formation( String nom, String url) {
                this.nom = nom;
                this.url = url;
        
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

  

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

 
    public Date getDate() {
        return date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

 
    public void setDate(Date date) {
        this.date = date;
    }


 
}
