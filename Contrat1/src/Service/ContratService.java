/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;
import java.sql.*;
import Entities.Contrat;
import Utils.ConnexionBD;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class ContratService {
      Connection c =ConnexionBD.getInstance().getConn();
      public void ajoutercontrat (Contrat c1)
      {
          try {
              Statement st = c.createStatement();
String req = "insert into contrat(id_freelance,id_entreprise,tache,prix,durée,reponse) values ('"+c1.getId_freelance()+"','"+c1.getId_entreprise()+"','"+c1.getTache()+"','"+c1.getPrix()+"','"+c1.getDurée()+"','"+c1.getReponse()+"')";

              st.execute(req);
          } catch (SQLException ex) {
              Logger.getLogger(ContratService.class.getName()).log(Level.SEVERE, null, ex);
          }
      }
      public void suppr ( Contrat c1 )
    {
        try {
            PreparedStatement pt = c.prepareStatement("delete from contrat where id=? ");
            pt.setInt(1, c1.getId());
            pt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ContratService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
       public void ModifierTzche ( int id,String tache ) throws SQLException 
       {
         PreparedStatement pt = c.prepareStatement("update contrat set tache=? where id=? ");
            pt.setString(1, tache);
            pt.setInt(2, id);
            pt.executeUpdate();
       
       }
       public void Modifierduré ( int id,int duré ) throws SQLException 
       {
         PreparedStatement pt = c.prepareStatement("update contrat set durée=? where id=? ");
            pt.setInt(1, duré);
            pt.setInt(2, id);
            pt.executeUpdate();
       
       }
       public void ModifierPrix ( int id,float prix ) throws SQLException 
       {
         PreparedStatement pt = c.prepareStatement("update contrat set prix=? where id=? ");
            pt.setFloat(1, prix);
            pt.setInt(2, id);
            pt.executeUpdate();
       
       }
       public int Affiché (String nom)
       {
        int r= -1 ;
           try {
              Statement st = c.createStatement();
              String req = "SELECT id FROM fos_user where username='"+nom+"'";
              PreparedStatement pt = c.prepareStatement(req);
          
              ResultSet rs = st.executeQuery(req);
              while (rs.next())
              {  r = rs.getInt(1);
                      }
          } catch (SQLException ex) {
              Logger.getLogger(ContratService.class.getName()).log(Level.SEVERE, null, ex);
          }
          return r ; 
       }
       public String AffichEntreprise (int id )
       {
          String r=""; try {
             
              Statement st = c.createStatement();
              String req = "SELECT * FROM fos_user where id='"+id+"'";
              PreparedStatement pt = c.prepareStatement(req);
          
              ResultSet rs = st.executeQuery(req);
              while (rs.next())
              {  r = rs.getString(20);
              }
          } catch (SQLException ex) {
              Logger.getLogger(ContratService.class.getName()).log(Level.SEVERE, null, ex);
          }
           
           return r ; 
           
       }
        public ArrayList<Contrat> afiiche() throws SQLException {
        ArrayList<Contrat> myList = new ArrayList();
        Statement st = c.createStatement();
        String req = "select * from contrat";
        ResultSet rs = st.executeQuery(req);
        while (rs.next()) {
            Contrat t;

            t = new Contrat(rs.getInt(1),AffichEntreprise(rs.getInt(3)),rs.getInt(7) );
            
            myList.add(t);

        }
        return myList;
    }
         public void listdescontrat(int id ) throws SQLException, FileNotFoundException, DocumentException, IOException {
        String r= ""; 
        Statement st = c.createStatement();
        String req = "select * from contrat where id='"+id+"'";
        ResultSet rs = st.executeQuery(req);
         while (rs.next()) {
             Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Contrat.pdf"));
                    document.open();
                   
                    document.add(new Paragraph("Article 1: Liste Des taches :\n ",FontFactory.getFont(FontFactory.TIMES_BOLD,24,Font.BOLD,BaseColor.RED)));
                     document.add(new Paragraph(rs.getString(4),FontFactory.getFont(FontFactory.TIMES_BOLD,18,Font.NORMAL,BaseColor.BLACK)));
                      document.add(new Paragraph("\n Article 2 : Les sousigné :\n ",FontFactory.getFont(FontFactory.TIMES_BOLD,24,Font.BOLD,BaseColor.RED)));
                        document.add(new Paragraph("nom Freelancer : ",FontFactory.getFont(FontFactory.TIMES_BOLD,20,Font.NORMAL,BaseColor.GREEN)));
                          document.add(new Paragraph(AffichEntreprise(rs.getInt(2)),FontFactory.getFont(FontFactory.TIMES_BOLD,18,Font.NORMAL,BaseColor.BLACK)));
                           document.add(new Paragraph("nom Entreprise : ",FontFactory.getFont(FontFactory.TIMES_BOLD,20,Font.NORMAL,BaseColor.GREEN)));
                            document.add(new Paragraph(AffichEntreprise(rs.getInt(3)),FontFactory.getFont(FontFactory.TIMES_BOLD,18,Font.NORMAL,BaseColor.BLACK)));
                             document.add(new Paragraph("\n Article 3:duréé (en jours) :\n ",FontFactory.getFont(FontFactory.TIMES_BOLD,24,Font.BOLD,BaseColor.RED)));
                              document.add(new Paragraph(String.valueOf(rs.getInt(6)),FontFactory.getFont(FontFactory.TIMES_BOLD,18,Font.NORMAL,BaseColor.BLACK)));
                                document.add(new Paragraph("\n Article 4:Prix \n",FontFactory.getFont(FontFactory.TIMES_BOLD,24,Font.BOLD,BaseColor.RED)));
                                 document.add(new Paragraph(String.valueOf(rs.getFloat(5)),FontFactory.getFont(FontFactory.TIMES_BOLD,18,Font.NORMAL,BaseColor.BLACK)));
                             document.close();
                    String[] parametres = new String[]{
                        "C:\\Program Files (x86)\\Adobe\\Acrobat Reader DC\\Reader\\AcroRd32.exe",
                        "Contrat.pdf"
                    };
                    Runtime runtime = Runtime.getRuntime();
                     
                    runtime.exec(parametres);
             
           

        }
         
}
public void accepter (int id )
             {
          try {
              PreparedStatement pt = c.prepareStatement("update contrat set reponse='"+1+"' where id='"+id+"' ");
              
              pt.executeUpdate();
          } catch (SQLException ex) {
              Logger.getLogger(ContratService.class.getName()).log(Level.SEVERE, null, ex);
          }
             
             
             }

public void sup (int id )
             {
          try {
              PreparedStatement pt = c.prepareStatement("delete from contrat where id='"+id+"' ");
              
              pt.executeUpdate();
          } catch (SQLException ex) {
              Logger.getLogger(ContratService.class.getName()).log(Level.SEVERE, null, ex);
          }}
     public ArrayList<Contrat> afiicheT() throws SQLException 
     {
        ArrayList<Contrat> myList = new ArrayList();
        Statement st = c.createStatement();
        String req = "select * from contrat";
        ResultSet rs = st.executeQuery(req);
        while (rs.next()) {
            Contrat t;

            t = new Contrat(rs.getInt(1),AffichEntreprise(rs.getInt(3)),rs.getInt(6),rs.getInt(7) );
            
            myList.add(t);

        }        
              return myList ;  
             }

}
    

