/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Contrat;
import Service.ContratService;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class ContratTraiterController implements Initializable {

    @FXML
    private TableView<Contrat> table;
    @FXML
    private TableColumn<Contrat,Integer> id;
    @FXML
    private TableColumn<Contrat,String> nom;
    @FXML
    private TableColumn<Contrat,Integer> d;
    @FXML
    private JFXButton re;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            
            ContratService srv = new ContratService();
            ArrayList<Contrat> r = srv.afiicheT() ;
          
            ObservableList<Contrat>  list = FXCollections.observableArrayList();
            for (int i = 0; i < r.size(); i++)
            {
                if(r.get(i).getReponse()==1)
                { Contrat t = new Contrat();
                
                t.setId(r.get(i).getId());
                t.setNom(r.get(i).getNomEntreprise());
                t.setDurée(r.get(i).getDurée());
                list.add(t);}
                
            }
           
            System.err.println(list);
            id.setCellValueFactory(new PropertyValueFactory<Contrat,Integer>("id")); 
            nom.setCellValueFactory(new PropertyValueFactory<Contrat,String>("nom"));
            d.setCellValueFactory(new PropertyValueFactory<Contrat,Integer>("durée")); 
            table.setItems(list);
        } catch (SQLException ex) {
            Logger.getLogger(ContratTraiterController.class.getName()).log(Level.SEVERE, null, ex);
        }
            
       
    }       

    @FXML
    private void retour(ActionEvent event) throws IOException {
           FXMLLoader load1 = new FXMLLoader(getClass().getResource("/Views/Consultercontrat.fxml")); 
           Parent root = load1.load(); 
           ConsultercontratController irc = load1.getController();
          
           re.getScene().setRoot(root);
    }
    
}
