/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
 
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import Entities.Contrat;
import Service.ContratService;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sun.security.krb5.internal.crypto.Des;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class ConsultercontratController implements Initializable {

    @FXML
    private Label btnexit;
    @FXML
    private JFXButton btnaj;
    @FXML
    private JFXButton btn;
    @FXML
    private JFXTextField txtid;
    @FXML
    private JFXButton btn1;
    @FXML
    private JFXButton btn11;
    @FXML
    private TableView<Contrat> table;
    @FXML
    private TableColumn<Contrat,Integer> id;
    @FXML
    private TableColumn<Contrat,String> nom;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ContratService srv = new ContratService();
            ArrayList<Contrat> r = srv.afiiche() ;
          
            ObservableList<Contrat>  list = FXCollections.observableArrayList();
            for (int i = 0; i < r.size(); i++)
            {
                if(r.get(i).getReponse()==0)
                { Contrat t = new Contrat();
                
                t.setId(r.get(i).getId());
                t.setNom(r.get(i).getNomEntreprise());
                
                list.add(t);}
                
            }
           
            System.err.println(list);
            id.setCellValueFactory(new PropertyValueFactory<Contrat,Integer>("id")); 
            nom.setCellValueFactory(new PropertyValueFactory<Contrat,String>("nom"));
            table.setItems(list);
            
        } catch (SQLException ex) {
            Logger.getLogger(ConsultercontratController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @FXML
    private void Action(MouseEvent event) {
    }
Stage primaryStage = new Stage();
    @FXML
    private void ajouterT(ActionEvent event) throws IOException {
        FXMLLoader load1 = new FXMLLoader(getClass().getResource("ContratTraiter.fxml")); 
           Parent root = load1.load(); 
           ContratTraiterController irc = load1.getController();
           
           btnexit.getScene().setRoot(root);
    }

    @FXML
    private void acp(ActionEvent event) {
        try {
            ContratService srv = new ContratService();
            srv.accepter(Integer.parseInt(txtid.getText()));
            ArrayList<Contrat> r = srv.afiiche() ;
            ObservableList<Contrat>  list = FXCollections.observableArrayList();
            for (int i = 0; i < r.size(); i++)
            {
                if(r.get(i).getReponse()==0)
                { Contrat t = new Contrat();
                
                t.setId(r.get(i).getId());
                t.setNom(r.get(i).getNomEntreprise());
                 
                list.add(t);}
                
            }
           
            System.err.println(list);
            id.setCellValueFactory(new PropertyValueFactory<Contrat,Integer>("id")); 
            nom.setCellValueFactory(new PropertyValueFactory<Contrat,String>("nom"));
            table.setItems(list);
        } catch (SQLException ex) {
            Logger.getLogger(ConsultercontratController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void ref(ActionEvent event) {
        try {
            ContratService srv = new ContratService();
            srv.sup(Integer.parseInt(txtid.getText()));
            ArrayList<Contrat> r = srv.afiiche() ;
            ObservableList<Contrat>  list = FXCollections.observableArrayList();
            for (int i = 0; i < r.size(); i++)
            {
                if(r.get(i).getReponse()==0)
                { Contrat t = new Contrat();
                
                t.setId(r.get(i).getId());
                t.setNom(r.get(i).getNomEntreprise());
                 
                list.add(t);}
                
            }
           
            System.err.println(list);
            id.setCellValueFactory(new PropertyValueFactory<Contrat,Integer>("id")); 
            nom.setCellValueFactory(new PropertyValueFactory<Contrat,String>("nom"));
            table.setItems(list);
        } catch (SQLException ex) {
            Logger.getLogger(ConsultercontratController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void consulter(ActionEvent event) throws FileNotFoundException, DocumentException, IOException, SQLException {
         ContratService srv = new ContratService();
            srv.listdescontrat(Integer.parseInt(txtid.getText()));
           
       
         
      
    }
    
}
