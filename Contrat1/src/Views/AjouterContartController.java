/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Contrat;
import Service.ContratService;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class AjouterContartController implements Initializable {

    @FXML
    private JFXTextField nomE;
    @FXML
    private JFXTextField nomF;
    @FXML
    private JFXTextArea tache;
    @FXML
    private JFXTextField prix;
    @FXML
    private JFXTextField durée;
    @FXML
    private JFXButton ajouter;
    @FXML
    private Label btnexit;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void ajoutercontrat(ActionEvent event) {
        ContratService srv = new ContratService() ; 
        int e = srv.Affiché(nomE.getText()); 
        int f = srv.Affiché(nomF.getText()); 
        Contrat c = new Contrat(f,e,0,Integer.parseInt(durée.getText()),tache.getText(),Float.parseFloat(prix.getText()));
        srv.ajoutercontrat(c);
        nomE.setText("");
        nomF.setText("");
        tache.setText("");
        prix.setText("");
        durée.setText("");
        
    }

    @FXML
    private void Action(MouseEvent event) {
       System.exit(0);
    }
    
}
