/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author Asus
 */
public class Contrat {
    	int id,id_freelance,id_entreprise,reponse,durée;
        String tache,nom , nomF;
        float prix ;

    public Contrat(int id, String nom,int durée,int reponse) {
        this.id = id;
        this.reponse = reponse;
        this.durée = durée;
        this.nom = nom;
    }

    public Contrat(int id, int durée, String nom) {
        this.id = id;
        this.durée = durée;
        this.nom = nom;
    }

    public Contrat(int durée, String tache, String nom, String nomF, float prix) {
        this.durée = durée;
        this.tache = tache;
        this.nom = nom;
        this.nomF = nomF;
        this.prix = prix;
    }

    public void setDurée(int durée) {
        this.durée = durée;
    }
         
    public String getNomF() {
        return nomF;
    }

    public void setNomF(String nomF) {
        this.nomF = nomF;
    }

    public Contrat(int id, String nom, int reponse) {
        this.id = id;
        this.reponse = reponse;
        this.nom = nom;
    }

    public Contrat(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Contrat() {
    }

    public String getNomEntreprise() {
        return nom;
    }

    public void setNomEntreprise(String nomEntreprise) {
        this.nom = nomEntreprise;
    }

    public Contrat(int id, int id_freelance, int id_entreprise, int reponse, int durée, String tache, float prix) {
        this.id = id;
        this.id_freelance = id_freelance;
        this.id_entreprise = id_entreprise;
        this.reponse = reponse;
        this.durée = durée;
        this.tache = tache;
        this.prix = prix;
    }

    public Contrat( int id_freelance, int id_entreprise, int reponse, int durée, String tache, float prix) {
      
        this.id_freelance = id_freelance;
        this.id_entreprise = id_entreprise;
        this.reponse = reponse;
        this.durée = durée;
        this.tache = tache;
        this.prix = prix;
    }

   
    

    public int getDurée() {
        return durée;
    }



    public int getId() {
        return id;
    }

    public int getId_freelance() {
        return id_freelance;
    }

    public int getId_entreprise() {
        return id_entreprise;
    }

    public String getTache() {
        return tache;
    }

    public int getReponse() {
        return reponse;
    }

    public float getPrix() {
        return prix;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setId_freelance(int id_freelance) {
        this.id_freelance = id_freelance;
    }

    public void setId_entreprise(int id_entreprise) {
        this.id_entreprise = id_entreprise;
    }

    public void setTache(String tache) {
        this.tache = tache;
    }

    public void setReponse(int reponse) {
        this.reponse = reponse;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    @Override
    public String toString() {
        return "Contrat{" + "id=" + id + ", id_freelance=" + id_freelance + ", id_entreprise=" + id_entreprise + ", reponse=" + reponse + ", dur\u00e9e=" + durée + ", tache=" + tache + ", nomEntreprise=" + nom + ", prix=" + prix + '}';
    }

    
        
}
