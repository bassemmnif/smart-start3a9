<?php

namespace ProjetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contrat
 *
 * @ORM\Table(name="contrat")
 * @ORM\Entity
 */
class Contrat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="ProjetBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_entreprise", referencedColumnName="id")
     * })
     */
    private $idEntreprise;
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="ProjetBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_freelance", referencedColumnName="id")
     * })
     */
    private $idFreelance;

    /**
     * @var string
     *
     * @ORM\Column(name="tache", type="string", length=255, nullable=false)
     */
    private $tache;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", precision=10, scale=0, nullable=false)
     */
    private $prix;
    /**
     * @var boolean
     *
     * @ORM\Column(name="reponse", type="boolean", nullable=true)
     */
    private $reponse;
    /**
     * @var string     *
     * @ORM\Column(name="datedebut", type="date", nullable=true)
     */
    private $datedebut;
    /**
     * @var string     *
     * @ORM\Column(name="datefin", type="date", nullable=true)
     */
    private $datefint;

    /**
     * @return string
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * @param string $datedebut
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;
    }

    /**
     * @return string
     */
    public function getDatefint()
    {
        return $this->datefint;
    }

    /**
     * @param string $datefint
     */
    public function setDatefint($datefint)
    {
        $this->datefint = $datefint;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getIdFreelance()
    {
        return $this->idFreelance;
    }

    /**
     * @param User $idFreelance
     */
    public function setIdFreelance($idFreelance)
    {
        $this->idFreelance = $idFreelance;
    }

    /**
     * @return User
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * @param User $idEntreprise
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;
    }




    /**
     * Set tache
     *
     * @param string $tache
     *
     * @return Contrat
     */
    public function setTache($tache)
    {
        $this->tache = $tache;

        return $this;
    }

    /**
     * Get tache
     *
     * @return string
     */
    public function getTache()
    {
        return $this->tache;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return Contrat
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set duree
     *
     * @param integer $duree
     *
     * @return Contrat
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return integer
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set reponse
     *
     * @param boolean $reponse
     *
     * @return Contrat
     */
    public function setReponse($reponse)
    {
        $this->reponse = $reponse;

        return $this;
    }

    /**
     * Get reponse
     *
     * @return boolean
     */
    public function getReponse()
    {
        return $this->reponse;
    }
}
