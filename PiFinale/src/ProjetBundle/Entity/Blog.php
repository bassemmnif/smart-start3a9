<?php

namespace ProjetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Blog
 *
 * @ORM\Table(name="blog", indexes={@ORM\Index(name="id_freelance", columns={"id_freelance"})})
 * @ORM\Entity
 */
class Blog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=false)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="redaction", type="string", length=255, nullable=false)
     */
    private $redaction;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrVue", type="integer", nullable=false)
     */
    private $nbrvue;

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string", length=255, nullable=false)
     */
    private $etat;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", length=255, nullable=false)
     */
    private $categorie;
    /**
     * @var \FosUser
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_freelance", referencedColumnName="id")
     * })
     */
    private $idFreelance;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Blog
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set redaction
     *
     * @param string $redaction
     *
     * @return Blog
     */
    public function setRedaction($redaction)
    {
        $this->redaction = $redaction;

        return $this;
    }

    /**
     * Get redaction
     *
     * @return string
     */
    public function getRedaction()
    {
        return $this->redaction;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Blog
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set nbrvue
     *
     * @param integer $nbrvue
     *
     * @return Blog
     */
    public function setNbrvue($nbrvue)
    {
        $this->nbrvue = $nbrvue;

        return $this;
    }

    /**
     * Get nbrvue
     *
     * @return integer
     */
    public function getNbrvue()
    {
        return $this->nbrvue;
    }

    /**
     * Set etat
     *
     * @param string $etat
     *
     * @return Blog
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set categorie
     *
     * @param string $categorie
     *
     * @return Blog
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set idFreelance
     *
     * @param integer $idFreelance
     *
     * @return Blog
     */
    public function setIdFreelance($idFreelance)
    {
        $this->idFreelance = $idFreelance;

        return $this;
    }

    /**
     * Get idFreelance
     *
     * @return integer
     */
    public function getIdFreelance()
    {
        return $this->idFreelance;
    }
}
