<?php

namespace ProjetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Conversation
 *
 * @ORM\Table(name="conversation", indexes={@ORM\Index(name="id1", columns={"id1"}), @ORM\Index(name="id2", columns={"id2"})})
 * @ORM\Entity
 */
class Conversation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     */
    private $id1;

    /**
     * @var integer
     *
     * @ORM\Column(name="id2", type="integer", nullable=false)
     */
    private $id2;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="seen", type="boolean", nullable=false)
     */
    private $seen;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="seen_date", type="datetime", nullable=false)
     */
    private $seenDate;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id1
     *
     * @param integer $id1
     *
     * @return Conversation
     */
    public function setId1($id1)
    {
        $this->id1 = $id1;

        return $this;
    }

    /**
     * Get id1
     *
     * @return integer
     */
    public function getId1()
    {
        return $this->id1;
    }

    /**
     * Set id2
     *
     * @param integer $id2
     *
     * @return Conversation
     */
    public function setId2($id2)
    {
        $this->id2 = $id2;

        return $this;
    }

    /**
     * Get id2
     *
     * @return integer
     */
    public function getId2()
    {
        return $this->id2;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Conversation
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set seen
     *
     * @param boolean $seen
     *
     * @return Conversation
     */
    public function setSeen($seen)
    {
        $this->seen = $seen;

        return $this;
    }

    /**
     * Get seen
     *
     * @return boolean
     */
    public function getSeen()
    {
        return $this->seen;
    }

    /**
     * Set seenDate
     *
     * @param \DateTime $seenDate
     *
     * @return Conversation
     */
    public function setSeenDate($seenDate)
    {
        $this->seenDate = $seenDate;

        return $this;
    }

    /**
     * Get seenDate
     *
     * @return \DateTime
     */
    public function getSeenDate()
    {
        return $this->seenDate;
    }
}
