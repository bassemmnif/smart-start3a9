<?php

namespace ProjetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Test
 *
 * @ORM\Table(name="test", indexes={@ORM\Index(name="id_f", columns={"id_f"})})
 * @ORM\Entity
 */
 class Test
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="qes", type="string", length=255, nullable=false)
     */
    public $qes;

    /**
     * @var string
     *
     * @ORM\Column(name="reponse", type="string", length=255, nullable=false)
     */
    public $reponse;

    /**
     * @var \Formation
     *
     * @ORM\ManyToOne(targetEntity="Formation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_f", referencedColumnName="id")
     * })
     */
    public $idF;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qes
     *
     * @param string $qes
     *
     * @return Test
     */
    public function setQes($qes)
    {
        $this->qes = $qes;

        return $this;
    }

    /**
     * Get qes
     *
     * @return string
     */
    public function getQes()
    {
        return $this->qes;
    }

    /**
     * Set reponse
     *
     * @param string $reponse
     *
     * @return Test
     */
    public function setReponse($reponse)
    {
        $this->reponse = $reponse;

        return $this;
    }

    /**
     * Get reponse
     *
     * @return string
     */
    public function getReponse()
    {
        return $this->reponse;
    }

    /**
     * Set idF
     *
     * @param \ProjetBundle\Entity\Formation $idF
     *
     * @return Test
     */
    public function setIdF(\ProjetBundle\Entity\Formation $idF = null)
    {
        $this->idF = $idF;

        return $this;
    }

    /**
     * @return \Formation
     */
    public function getIdF()
    {
        return $this->idF;
    }


}
