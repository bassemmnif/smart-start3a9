<?php

namespace ProjetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class indexController extends Controller
{
    public function acceuilAction()
    {
        return $this->render('@Projet/Default/Index.html.twig');
    }
    public function logAction()
    {
        $authChecker = $this->container->get('security.authorization_checker');
        $router = $this->container->get('router');

        if ($authChecker->isGranted('ROLE_ADMIN')) {
            return new RedirectResponse($router->generate('aceuillAdmin'), 307);

        }

        if ($authChecker->isGranted('ROLE_USER')) {
            return new RedirectResponse($router->generate('acceuil'), 307);
        }
        return $this->redirectToRoute('acceuil');

    }

}
