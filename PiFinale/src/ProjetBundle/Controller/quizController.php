<?php

namespace ProjetBundle\Controller;

use Doctrine\ORM\EntityRepository;
use ProjetBundle\Entity\Post;
use ProjetBundle\Entity\Test;
use ProjetBundle\Entity\Formation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use ProjetBundle\Repository\QuizRepository;
class quizController extends Controller
{
    public function quizAction($id,Request $request)
    {
        $quiz = new Test();
        $post = $this->getDoctrine()->getRepository('ProjetBundle:Test')->findBy(array('idF' => $id));

             foreach ($post as $p )
        {$n[$p->getQes()]=$p->getQes();

        }
        $form = $this->createFormBuilder($post)
            ->add('qes' ,ChoiceType::class,array("choices"=>$n))

            ->add('reponse', TextareaType::class, array('attr' => array('class' => 'form-control','required' => true)))
            ->add('check', SubmitType::class, array( 'attr' => array('class' => 'btn btn-info' )))
            ->getForm();
        $form->handleRequest($request);

       if ($form->isSubmitted()) {
           $res = $this->getDoctrine()->getRepository('ProjetBundle:Test')->findBy(array('qes' =>$form['qes']->getData() ));
           foreach ($res as $r )
           {
               if ($r->getReponse()==$form['reponse']->getData())
               {
                  $this->addFlash(
                      "success","Reponse correct!!"
                  );
               }
               else
               {
                   $this->addFlash(
                       "success","Reponse incorrect!!"
                   );
               }

           }
       }


        return $this->render('@Projet/Default/quiz.html.twig',array("form" => $form->createView(),'post'=> $post)
        );
    }
    public function AjouterquizAction(Request $request)
    {
        $quiz = new Test();
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Formation')->findAll();
        $form = $this->createFormBuilder($quiz)
            ->add('idF' , EntityType::class,
                array("class"=>"ProjetBundle\Entity\Formation","choice_label"=>"nom","multiple"=>false))
        ->add('qes', TextType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "question"))
        ->add('reponse', TextareaType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "reponse"))
        ->add('Ajouter', SubmitType::class, array( 'attr' => array('class' => 'btn btn-info' )))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted()) {

           $quiz ->getIdF($form['idF']->getData());
            $quiz ->getReponse($form['reponse']->getData());
            $quiz ->getQes($form['qes']->getData());
            $em = $this->getDoctrine()->getManager();
            $em->persist($quiz);
            $em->flush();
            return $this->redirectToRoute("ajouterquiz");

        }
        return $this->render('@Projet/Default/admin/ajouterQuiz.html.twig',array("form" => $form->createView())
        );
    }


public function consulterQuizAction(Request $request)
{
    $con = $this -> getDoctrine()->getRepository('ProjetBundle:Test')->findAll();


    return $this->render('@Projet/Default/admin/consulterQuiz.html.twig',['con'=> $con]);
}
    public function suprQuizAction($id)
    {
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Test')->find($id);

        $em= $this->getDoctrine()->getManager();
        $em->remove($con);
        $em->flush();
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Test')->findAll();
        return $this->render('@Projet/Default/admin/consulterQuiz.html.twig',['con'=> $con]);
    }
    public function modifierQuizAction(Request $request,$id)
    {
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Test')->find($id);
        $con->setIdF($con->getIdF());
        $con->setReponse($con->getReponse());
        $con->setQes($con->getQes());

        $form = $this->createFormBuilder($con)

            ->add('idF' , EntityType::class,
                array("class"=>"ProjetBundle\Entity\Formation","choice_label"=>"nom","multiple"=>false))
            ->add('qes', TextType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "qestion"))
            ->add('reponse', TextareaType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "reponse"))
            ->add('Modifier', SubmitType::class, array( 'attr' => array('class' => 'btn btn-info' )))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $con ->getIdF($form['idF']->getData());
            $con ->getReponse($form['reponse']->getData());
            $con ->getQes($form['qes']->getData());

            $em = $this->getDoctrine()->getManager();

            $em->flush();
            $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:Test')->findAll();
            return $this->redirectToRoute("consulterQuiz",['con'=> $con2]);
        }
        return $this->render('@Projet/Default/admin/modifierQuiz.html.twig',array("form" => $form->createView()));

    }
}
