<?php

namespace ProjetBundle\Controller;

use ProjetBundle\Entity\Formation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use ProjetBundle\Repository\FormationRepository ;

class FormationAdminController extends Controller
{
    public function ajouterFormationAction(Request $request)
    {
        $contrat = new Formation();
        $form = $this->createFormBuilder($contrat)

            ->add('imag', ChoiceType::class, [
                'choices'  => [
                    'hard skills' => 'Bundles/Pack/images/hardSkills.jpg',
                    'Soft skills' => 'Bundles/Pack/images/softSkills.jpg',

                ],
            ])
            ->add('nom', TextType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "nom"))
            ->add('url1', TextType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "url"))
            ->add('descr', TextareaType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "description"))
        ->add('Ajouter', SubmitType::class, array( 'attr' => array('class' => 'btn btn-info' )))

        ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $contrat ->setVu(0);
            $contrat->setDate(new \DateTime('now'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($contrat);
            $em->flush();
            return $this->redirectToRoute("ajouterFormation");
        }
        return $this->render('@Projet/Default/admin/ajouterFormation.html.twig',array("form" => $form->createView())
        );

    }
    public function consulterFormationAction(Request $request)
    {
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Formation')->findAll();
        return $this->render('@Projet/Default/admin/consulterFormation.html.twig',['con'=> $con]);
    }
    public function supfrAction($id)
    {   $con = $this -> getDoctrine()->getRepository('ProjetBundle:Formation')->find($id);

        $em= $this->getDoctrine()->getManager();
        $em->remove($con);
        $em->flush();
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Formation')->findAll();
        return $this->render('@Projet/Default/admin/consulterFormation.html.twig',['con'=> $con]);

    }
    public function modifierfrAction(Request $request,$id)
    {
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Formation')->find($id);
        $con->setImag($con->getImag());
        $con->setNom($con->getNom());
        $con->setUrl1($con->getUrl1());
        $con->setDescr($con->getDescr());
        $form = $this->createFormBuilder($con)

            ->add('imag', ChoiceType::class, [
                'choices'  => [
                    'hard skills' => 'Bundles/Pack/images/hardSkills.jpg',
                    'Soft skills' => 'Bundles/Pack/images/softSkills.jpg',

                ],
            ])
            ->add('nom', TextType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "nom"))
            ->add('url1', TextType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "url"))
            ->add('descr', TextareaType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "description"))
            ->add('Modifier', SubmitType::class, array( 'attr' => array('class' => 'btn btn-info' )))
        ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $con->setDate(new \DateTime('now'));

            $em = $this->getDoctrine()->getManager();

            $em->flush();
            $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:Formation')->findAll();
            return $this->redirectToRoute("consulterFormation",['con'=> $con2]);
        }
        return $this->render('@Projet/Default/admin/ModifierFormation.html.twig',array("form" => $form->createView()));

    }
    public  function  rechfAction (Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $requestString = $request->get('q');
        $posts = $em ->getRepository('ProjetBundle:Formation')->findEntitiesByString($requestString);
        if (!$posts)
        {
            $result['posts']['error'] = 'formation pas disponible';
        }
        else
        {
         $request['posts']=$this->getRealEntities($posts);
        }
        return new Response(json_encode($result));
    }
    public function getRealEntities($posts)
    {
        foreach ($posts as $p) {
            $realEntities[$p->getId] = [$p->getId, $p->getDescr(), $p->getImag(), $p->getNom()];
        }
        return $realEntities;
    }

}
