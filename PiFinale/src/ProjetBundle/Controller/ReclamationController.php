<?php

namespace ProjetBundle\Controller;

use ProjetBundle\Entity\Reclammation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;



class ReclamationController extends Controller
{
    public function ajouterreclamtionAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getuser() ;

        $rec = new Reclammation();
        $form = $this->createFormBuilder($rec)

            ->add('objet', TextType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "Objet"))
            ->add('message', TextareaType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "message"))
            ->add('envoyer', SubmitType::class, array( 'attr' => array('class' => 'template-btn', )))
        ->getForm();
        $form->handleRequest($request);
        $users = $this -> getDoctrine()->getRepository('ProjetBundle:User')->findAll();

        if ($form->isSubmitted()) {

                    $rec->setIdUser($user);

            $rec->setDate(new \DateTime('now'));
            $rec->setEtat('encours');
            $em = $this->getDoctrine()->getManager();
            $em->persist($rec);
            $em->flush();
            return $this->redirectToRoute("ajouterreclamtion");

        }
        return $this->render('@Projet/Default/AjouterReclamtion.html.twig',array("form" => $form->createView())
        );
    }
    public function mesReclamtionAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getuser() ;
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Reclammation')->findBy(array('id_user'=>$user));
        $post  = $this->get('knp_paginator')->paginate(
            $con,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            4/*nbre d'éléments par page*/
        );
        return $this->render('@Projet/Default/mesreclamation.html.twig',['con'=> $post]
        );

    }
    public function suppReclamtionAction($id)
    {
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Reclammation')->find($id);
        $em= $this->getDoctrine()->getManager();
        $em->remove($con);
        $em->flush();
        $user = $this->get('security.token_storage')->getToken()->getuser() ;
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Reclammation')->findBy(array('id_user'=>$user));
        return $this->render('@Projet/Default/mesreclamation.html.twig',['con'=> $con]
        );

    }
    public function ReclamtionAdminAction()
    {

        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Reclammation')->findAll();
        return $this->render('@Projet/Default/admin/reclamtion.html.twig',['con'=> $con]
        );

    }
    public function RepondreAction($id,Request $request)
    {
        $rec = new Reclammation();
        $form = $this->createFormBuilder($rec)
            ->add('message', TextareaType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "message"))
            ->add('envoyer', SubmitType::class, array( 'attr' => array('class' => 'btn-info', )))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            $con = $this -> getDoctrine()->getRepository('ProjetBundle:Reclammation')->find($id);
            $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:User')->find($con->getIdUser());
            $message = (new \Swift_Message())

                ->setSubject($con->getObjet())
                ->setFrom('pifreedev19@gmail.com')
                ->setTo($con2->getEmail())
                ->setBody($form['message']->getData());

            $this->get('mailer')->send($message);
            $con->setEtat("traité");
            $em = $this->getDoctrine()->getManager();
            $em->persist($con);
            $em->flush();
            $con = $this -> getDoctrine()->getRepository('ProjetBundle:Reclammation')->findAll();
            return $this->render('@Projet/Default/admin/reclamtion.html.twig',['con'=> $con]);
        }

        return $this->render('@Projet/Default/admin/repondre.html.twig',array("form" => $form->createView())
        );

    }
}
