<?php

namespace ProjetBundle\Controller;

use ProjetBundle\Entity\Contrat;
use ProjetBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\Request;
use ProjetBundle\Repository\UserRepository ;
use Symfony\Component\HttpFoundation\Response;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;



class ContratController extends Controller
{
    public function ajoutercAction( Request $request)
    {
        $contrat = new Contrat();
        $form = $this->createFormBuilder($contrat)
            ->add('idFreelance', TextType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "nom Freelance"))
            ->add('idEntreprise', TextType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "nom Entreprise"))
            ->add('prix', TextType::class, array('attr' => array('class' => 'form-control','required' => true),'label' => "prix"))

            ->add('tache', TextareaType ::class, array('attr' => array('class' => 'form-control','required' => true,),'label' => "tache"))
            ->add('datedebut', DateType::class, [
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'yyyy-MM-dd',
            ])
            ->add('datefint', DateType::class, [
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'yyyy-MM-dd',
            ])
            ->add('Ajouter', SubmitType::class, array( 'attr' => array('class' => 'template-btn', )))

            ->getForm();
        $form->handleRequest($request);
        $users = $this -> getDoctrine()->getRepository('ProjetBundle:User')->findAll();



        if ($form->isSubmitted()) {
            $free = $form['idFreelance']->getData();
            $entr = $form['idEntreprise']->getData();

           foreach ($users as $u) {
               $name = $u->getUsername();
               if ($name == $entr)
                   $contrat->setIdEntreprise($u);
               if ($name == $free)
                   $contrat->setIdFreelance($u);

           }

            $datedebut = $form['datedebut']->getData();
            $datefint = $form['datefint']->getData();
            $prix = $form['prix']->getData();

            $tache = $form['tache']->getData();
            $contrat->setDatedebut($datedebut);
            $contrat->setDatefint($datefint);
            $contrat->setPrix($prix);

            $contrat->setTache($tache);
            $contrat->setReponse(false);

            $em = $this->getDoctrine()->getManager();
            $em->persist($contrat);
            $em->flush();
            return $this->redirectToRoute("ajouterc");
        }
        return $this->render('@Projet/Default/ajouterContrat.html.twig',array("form" => $form->createView())
        );
    }
    public function conscAction()

    {
        $user = $this->get('security.token_storage')->getToken()->getuser() ;
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Contrat')->findBy(array('idFreelance'=>$user));
        return $this->render('@Projet/Default/consultercontrat.html.twig',['con'=> $con]
        );
    }
    public function accepterAction($id)
    {   $con = $this -> getDoctrine()->getRepository('ProjetBundle:Contrat')->find($id);
        $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:User')->find($con->getIdEntreprise());
         $con ->setReponse(true);
        $em= $this->getDoctrine()->getManager();
        $em->flush();
        $message = (new \Swift_Message())
            ->setSubject("Etat du contrat")
            ->setFrom('pifreedev19@gmail.com')
            ->setTo($con2->getEmail())
            ->setBody("un contrat que vous avez a été accepter");
        $this->get('mailer')->send($message);
        $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:Contrat')->findAll();
        return $this->render('@Projet/Default/consultercontrat.html.twig',['con'=> $con2]);

    }
    public function refuserAction($id)
    {   $con = $this -> getDoctrine()->getRepository('ProjetBundle:Contrat')->find($id);
        $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:User')->find($con->getIdEntreprise());
        $message = (new \Swift_Message())
            ->setSubject("Etat du contrat")
            ->setFrom('pifreedev19@gmail.com')
            ->setTo($con2->getEmail())
            ->setBody("un contrat que vous avez a été Refuser voud devez cantacter le Freelancer ");
        $this->get('mailer')->send($message);
        $em= $this->getDoctrine()->getManager();
        $em->remove($con);
        $em->flush();
        $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:Contrat')->findAll();
        return $this->render('@Projet/Default/consultercontrat.html.twig',['con'=> $con2]);

    }
    public function pdfAction($id)
    {
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Contrat')->find($id);
        $snappy = $this->get("knp_snappy.pdf");
        $html = $this->renderView("@Projet/Default/pdfContrat.html.twig", array("con" =>$con ));
        $filename = "Contrat";
        return new Response($snappy->getOutputFromHtml($html),
            200,
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="%s"', $filename), 'encoding' => 'utf-8',
            ]
        );

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Contrat')->findBy(array('idFreelance'=>$user));
        return $this->render('@Projet/Default/consultercontrat.html.twig',['con'=> $con]
        );
    }


}
