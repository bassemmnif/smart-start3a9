<?php

namespace ProjetBundle\Controller;

use ProjetBundle\Entity\Candidature;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class mescandidatureController extends Controller
{
    public function mescandidatureAction(Request $request)
    {     $user = $this->get('security.token_storage')->getToken()->getuser() ;
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Candidature')->findBy(array('idFreelancer'=>$user));
        $post  = $this->get('knp_paginator')->paginate(
            $con,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            4/*nbre d'éléments par page*/
        );
        return $this->render('@Projet/Default/Mescondidature.html.twig',['con'=> $post]
        );

    }
    public function annulercandidatureAction($id)
    {
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Candidature')->find($id);
        $em= $this->getDoctrine()->getManager();
        $em->remove($con);
        $em->flush();
        $user = $this->get('security.token_storage')->getToken()->getuser() ;
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Candidature')->findBy(array('idFreelancer'=>$user));

        return $this->render('@Projet/Default/Mescondidature.html.twig',['con'=> $con]
        );
    }

}
