<?php

namespace ProjetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\ColumnChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;

class adminController extends Controller
{
    public function aceuillAdminAction()
    {
        /********************************Contrat signé **************************************************/
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Contrat')->findBy(array('reponse'=>1));
        $i=0 ;
        foreach ($con as $c  )
        {
            ++$i ;
        }
        /********************************Contrat non signé  **************************************************/
        $con1 = $this -> getDoctrine()->getRepository('ProjetBundle:Contrat')->findBy(array('reponse'=>0));
        $j=0 ;
        foreach ($con1 as $c  )
        {
            ++$j ;
        }
        /********************************Reclamation  **************************************************/
        $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:Reclammation')->findAll();
        $k=0 ;
        foreach ($con2 as $c  )
        {
            ++$k ;
        }
        /********************************Formation Hard Skills  **************************************************/
        $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:Formation')->findBy(array('imag'=>'Bundles/Pack/images/hardSkills.jpg'));
        $s=0 ;
        foreach ($con2 as $c  )
        {
            ++$s ;
        }
        /********************************Formation Sard Skills  **************************************************/
        $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:Formation')->findBy(array('imag'=>'Bundles/Pack/images/softSkills.jpg'));
        $h=0 ;
        foreach ($con2 as $c  )
        {
            ++$h ;
        }
        /********************************Candidature  **************************************************/
        $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:Candidature')->findAll();
        $t=0 ;
        foreach ($con2 as $c  )
        {
            ++$t ;
        }
        /********************************Nombre projet Freelance  **************************************************/
        $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:Projet')->findAll();
        $w=0 ;
        foreach ($con2 as $c  )
        {
            ++$w ;
        }
        /********************************Nombre Publications  **************************************************/
        $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:Publication')->findAll();
        $x=0 ;
        foreach ($con2 as $c  )
        {
            ++$x ;
        }
        /******************************** Nbre de vue / formation   **************************************************/
        $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:Formation')->findAll();
        $data=array();
        foreach ($con2 as $v)
        {
            $a = array(
                'formation'=> $v->getNom() ,
                'nombre de vue'=> $v->getVu()
            );
            array_push($data,$a);
        }

        $histogram = new ColumnChart();
        $histogram->getData()->setArrayToDataTable($data,true);

        $histogram->getOptions()->setTitle("nombre de vues:Formation");
        $histogram->getOptions()->setWidth(1000);
        $histogram->getOptions()->setHeight(800);
        $histogram->getOptions()->getLegend()->setPosition('none');
        $histogram->getOptions()->setColors(['#004394']);
        $histogram->getOptions()->getTitleTextStyle()->setBold(true);
        $histogram->getOptions()->getTitleTextStyle()->setColor('#009900');
        $histogram->getOptions()->getTitleTextStyle()->setItalic(true);
        $histogram->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $histogram->getOptions()->getTitleTextStyle()->setFontSize(20);
        /******************************** Reclamtion  **************************************************/

        $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:Reclammation')->findAll();
        $data2=array();
        $b=0 ;
        $d=0;
        foreach ($con2 as $v)
        {
            if($v->getEtat()=="encours")
            {++$b;}
            else
            {++$d;}
        }

        $f = array(
           array('etat'=> 'encours','nombre'=>$b),
            array('etat'=> 'traite','nombre'=>$d)
        );


        array_push($data2,$f);

        $histogram1 = new ColumnChart();
        $histogram1->getData()->setArrayToDataTable($f,true);

        $histogram1->getOptions()->setTitle("Reclamtions");
        $histogram1->getOptions()->setWidth(1000);
        $histogram1->getOptions()->setHeight(500);
        $histogram1->getOptions()->getLegend()->setPosition('none');
        $histogram1->getOptions()->setColors(['#004394']);
        $histogram1->getOptions()->getTitleTextStyle()->setBold(true);
        $histogram1->getOptions()->getTitleTextStyle()->setColor('#009900');
        $histogram1->getOptions()->getTitleTextStyle()->setItalic(true);
        $histogram1->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $histogram1->getOptions()->getTitleTextStyle()->setFontSize(20);

        /******************************** ProjetFreelance  **************************************************/

        $con2 = $this -> getDoctrine()->getRepository('ProjetBundle:Projet')->findAll();
        $data3=array();
        $l=0 ;
        $n=0;
        foreach ($con2 as $v)
        {
            if($v->getEtat()=="encours")
            {++$l;}
            else
            {++$n;}
        }

        $q = array(
            array('etat'=> 'encours','nombre'=>$l),
            array('etat'=> 'traite','nombre'=>$n)
        );


        array_push($data2,$f);

        $histogram2 = new ColumnChart();
        $histogram2->getData()->setArrayToDataTable($q,true);

        $histogram2->getOptions()->setTitle("Projets");
        $histogram2->getOptions()->setWidth(1000);
        $histogram2->getOptions()->setHeight(500);
        $histogram2->getOptions()->getLegend()->setPosition('none');
        $histogram2->getOptions()->setColors(['#004394']);
        $histogram2->getOptions()->getTitleTextStyle()->setBold(true);
        $histogram2->getOptions()->getTitleTextStyle()->setColor('#009900');
        $histogram2->getOptions()->getTitleTextStyle()->setItalic(true);
        $histogram2->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $histogram2->getOptions()->getTitleTextStyle()->setFontSize(20);
        $info = array(
            'contrats' => (string)$i,
            'contartns' => (string)$j,
            'Reclammation' => (string)$k,
            'FormationH' => (string)$s,
            'FormationS' => (string)$h,
            'can' => (string)$t,
            'Pro' => (string)$w,
            'Pub' => (string)$x
        );
        return $this->render('@Projet/Default/admin/acceuill.html.twig',array("nbr" => $info, 'histogram' => $histogram,'histogram1' => $histogram1,'histogram2' => $histogram2)
        );

    }
}

