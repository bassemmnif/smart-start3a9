<?php

namespace ProjetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MesprojetFController extends Controller
{
    public function MesProjetFAction(Request $request)
    { $user = $this->get('security.token_storage')->getToken()->getuser() ;
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Projet')->findBy(array('idFreelancer'=>$user));
        $post  = $this->get('knp_paginator')->paginate(
            $con,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            4/*nbre d'éléments par page*/
        );
        return $this->render('@Projet/Default/mesprojetF.html.twig',['con'=> $post]
        );
    }
    public function annulerProjetfAction($id)
    {
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Projet')->find($id);
        $em= $this->getDoctrine()->getManager();
        $em->remove($con);
        $em->flush();
        $user = $this->get('security.token_storage')->getToken()->getuser() ;
        $con = $this -> getDoctrine()->getRepository('ProjetBundle:Projet')->findBy(array('idFreelancer'=>$user));

        return $this->render('@Projet/Default/mesprojetF.html.twig',['con'=> $con]);
    }
}
