<?php

namespace ProjetBundle\Controller;

use Composer\DependencyResolver\Request;
use ProjetBundle\Entity\Publication;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class rechercheController extends Controller
{
    public function rechAction()
    {
     $post = $this -> getDoctrine()->getRepository('ProjetBundle:Publication')->findAll();
        return $this->render('@Projet/Default/recherche.html.twig',array('posts'=>$post)
        );
    }
}
