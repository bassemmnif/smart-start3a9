<?php

namespace ProjetBundle\Controller;

use ProjetBundle\Entity\cmt;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
class FormationController extends Controller
{
    public function formationAction(Request $request)
    {   $posts = $this -> getDoctrine()->getRepository('ProjetBundle:Formation')->findAll();
        $post  = $this->get('knp_paginator')->paginate(
            $posts,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            4/*nbre d'éléments par page*/
        );
        return $this->render('@Projet/Default/formation.html.twig',['posts'=> $post]
        );
    }
    public function HardAction(Request $request)
    {   $posts = $this -> getDoctrine()->getRepository('ProjetBundle:Formation')->findby(array('imag' => 'Bundles/Pack/images/hardSkills.jpg'));
        $post  = $this->get('knp_paginator')->paginate(
            $posts,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            4/*nbre d'éléments par page*/
        );
        return $this->render('@Projet/Default/formation.html.twig',['posts'=> $post]
        );
    }
    public function SoftAction(Request $request)
    {   $posts = $this -> getDoctrine()->getRepository('ProjetBundle:Formation')->findby(array('imag' => 'Bundles/Pack/images/softSkills.jpg'));
        $post  = $this->get('knp_paginator')->paginate(
            $posts,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            4/*nbre d'éléments par page*/
        );
        return $this->render('@Projet/Default/formation.html.twig',['posts'=> $post]
        );
    }
    public function courAction( $id,Request $request)
    {
        $post = $this -> getDoctrine()->getRepository('ProjetBundle:Formation')->find($id);
        $i = $post->getVu();
         $post->setVu(++$i);
         $post2= $this -> getDoctrine()->getRepository('ProjetBundle:cmt')->findBy(array('idF'=>$post->getId()));


        $em = $this->getDoctrine()->getManager();

        $em->flush();

        $comt = new cmt();
        $form = $this->createFormBuilder($comt)
            ->add('msg', TextareaType::class)
            ->add('Share',SubmitType::class, array('attr' => array('class' => 'btn btn-success green','required' => true)))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted())
        {
            $user = $this->get('security.token_storage')->getToken()->getuser() ;

                    $comt->setIdf($post);
                    $comt->setIdFreelancer($user);
                    $comt->setMsg($form['msg']->getData());

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($comt);
                    $em->flush();

            return $this->redirectToRoute('cour',array('id'=>$id));

            }






        return $this->render('@Projet/Default/cour.html.twig',['post2'=> $post2,'post'=> $post,"form" => $form->createView()]);

    }
}
