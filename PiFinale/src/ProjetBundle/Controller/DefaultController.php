<?php

namespace ProjetBundle\Controller;
use FOS\UserBundle\Model\User as BaseUser ;
use ProjetBundle\Entity\Candidature;
use ProjetBundle\Entity\Publication;
use ProjetBundle\Entity\User ;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $posts = $this -> getDoctrine()->getRepository('ProjetBundle:Publication')->findAll();
        $post  = $this->get('knp_paginator')->paginate(
            $posts,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            3/*nbre d'éléments par page*/
        );
        return $this->render('@Projet/Default/Index.html.twig',['posts'=> $post]
        );

    }
    public function appAction(Publication $publication)
    {
        $user = $this->get('security.token_storage')->getToken()->getuser() ;
        $candidature = new Candidature();
        $candidature -> setIdFreelancer($user);
        $candidature -> setIdProjet($publication);
        $candidature -> setDate(new \DateTime('now'));
        $candidature -> setEtat('non traite');

        $em = $this->getDoctrine()->getManager();

        $em->persist($candidature);
        $em->flush();

        $posts = $this -> getDoctrine()->getRepository('ProjetBundle:Publication')->findAll();
        return $this->render('@Projet/Default/Index.html.twig',['posts'=> $posts]
        );

    }



}



