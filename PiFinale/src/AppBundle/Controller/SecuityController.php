<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SecuityController extends Controller
{
    /**
     * @Route("/add")
     */
    public function addAction()
    {
        return $this->render('AppBundle:Secuity:add.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/")
     */
    public function redirectAction()
    {
        $authChecker = $this->container->get('security.authorization_checker');
        if ($authChecker->isGranted('ROLE_SUPER_ADMIN')) {
            return $this->render('@Projet/Default/admin/acceuill.html.twig');
        } else if ($authChecker->isGranted('ROLE_ENTREPRISE')) {
            return $this->render('@Projet/Default/Index.html.twig');
        } else if ($authChecker->isGranted('ROLE_FREELANCER')) {
            return $this->render('@Projet/Default/Index.html.twig');
        } else {
            return $this->render('@FOSUser/Security/login.html.twig');

        }


    }

}
