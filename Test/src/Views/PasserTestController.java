/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Service.ServiceTest;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class PasserTestController implements Initializable {

    @FXML
    private JFXComboBox<String> f;
    @FXML
    private Label qes;
    @FXML
    private JFXTextField rep;
    @FXML
    private JFXButton suiv;
    @FXML
    private JFXButton ter;
    int k = 0;
     int i = 0;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            ServiceTest srv = new ServiceTest();
            ArrayList<String> r = srv.listf();
            System.out.println(r);
            ObservableList<String> list = FXCollections.observableArrayList();
            for (int i = 0; i < r.size(); i++) {
                list.add(r.get(i));
            }
            f.setItems(list);
        } catch (SQLException ex) {
            Logger.getLogger(AjouterTestController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void list(ActionEvent event) throws SQLException {
        String value = (String) f.getSelectionModel().getSelectedItem();
        ServiceTest srv = new ServiceTest();
        int j = srv.idf(value);
        System.err.println(j);
        ArrayList<String> h = srv.listq(j);
        qes.setText("");
        qes.setText(h.get(k));
         
    }

    @FXML
    private void suivant(ActionEvent event) throws SQLException {
        String value = (String) f.getSelectionModel().getSelectedItem();
        ServiceTest srv = new ServiceTest();
        int j = srv.idf(value);
        ArrayList<String> r = srv.listr(j);
        ArrayList<String> h = srv.listq(j);
       
        int n = h.size();
       if (k <= n-1) {
            System.err.println(r.get(k));
            if (r.get(k).equals(rep.getText())) {
                i++;
                System.err.println(i);
            } 
            
            k++;
            String value1 = (String) f.getSelectionModel().getSelectedItem();
            ServiceTest srv1 = new ServiceTest();
            int j1 = srv1.idf(value1);
            ArrayList<String> h1 = srv.listq(j1);
            qes.setText(h1.get(k));rep.setText("");
        }
        else 
        {
         Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Test Connection");
        alert.setHeaderText("Results:");
        alert.setContentText(Integer.toString(i)+"/"+Integer.toString(r.size()));
 
        alert.showAndWait();
        }
    }

    @FXML
    private void terminer(ActionEvent event) {
        System.exit(0);
    }

}
