/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Test;
import Service.ServiceTest;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class AjouterTestController implements Initializable {

    @FXML
    private JFXComboBox<String> f;
    @FXML
    private JFXTextField q;
    @FXML
    private JFXTextField r;
    @FXML
    private JFXButton ajouter;
    @FXML
    private JFXButton rets;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ServiceTest srv = new ServiceTest();
            ArrayList<String> r = srv.listf() ;
            System.out.println(r);
            ObservableList<String>  list = FXCollections.observableArrayList();
            for (int i = 0; i < r.size(); i++)
            {
                list.add(r.get(i));
            }
            f.setItems(list);
        } catch (SQLException ex) {
            Logger.getLogger(AjouterTestController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }    

    @FXML
    private void ajoutert(ActionEvent event) {
         String value = (String)f.getSelectionModel().getSelectedItem();
        ServiceTest srv = new ServiceTest(); 
        int i = srv.idf(value); 
        System.out.println(i);
        Test t = new Test(i,q.getText(),r.getText());
        System.err.println(t.toString());
        srv.ajouterTest(t);
        r.setText("");
        q.setText("");
    }

    @FXML
    private void list(ActionEvent event) {
        
    }

    @FXML
    private void retour(ActionEvent event) throws IOException {
          FXMLLoader load1 = new FXMLLoader(getClass().getResource("/Views/TestAdmin.fxml")); 
           Parent root = load1.load(); 
           TestAdminController irc = load1.getController();
           
           rets.getScene().setRoot(root);
    }
    
}
