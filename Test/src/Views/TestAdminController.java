/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entities.Test;
import Service.ServiceTest;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class TestAdminController implements Initializable {

     @FXML
    private TableView<Test> table  ;
    @FXML
    private TableColumn<Test, Integer> id;
    @FXML
    private TableColumn<Test, String> qes ;
    @FXML
    private TableColumn<Test, String> Reponse ;
    @FXML
    private JFXButton btnaj;
    @FXML
    private JFXTextField txtid;
    @FXML
    private JFXButton btn;
    @FXML
    private TableColumn<Test, String> nom_Formation;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
            
         try {
             ServiceTest srv = new ServiceTest();
             ArrayList<Test> r = srv.afiiche() ;
             System.err.println(r);
             ObservableList<Test>  list = FXCollections.observableArrayList();
             for (int i = 0; i < r.size(); i++)
             {
                 Test t = new Test();
                 
                 t.setId(r.get(i).getId_f());
                 System.err.println(t.toString());
                 t.setQes(r.get(i).getQes());
                 t.setReponse(r.get(i).getReponse());
                 
                 list.add(t);
                 
             }
             System.err.println(list);
             
             
             id.setCellValueFactory(new PropertyValueFactory<Test,Integer>("id"));
             Reponse.setCellValueFactory(new PropertyValueFactory<Test,String>("Reponse"));
             qes.setCellValueFactory(new PropertyValueFactory<Test,String>("qes"));
             
             
             table.setItems(list);
         } catch (SQLException ex) {
             Logger.getLogger(TestAdminController.class.getName()).log(Level.SEVERE, null, ex);
         }
        
       
    }    
       
		Stage primaryStage = new Stage();

    @FXML
    private void ajouterT(ActionEvent event) throws IOException { 
        FXMLLoader load1 = new FXMLLoader(getClass().getResource("/Views/AjouterTest.fxml")); 
           Parent root = load1.load(); 
           AjouterTestController irc = load1.getController();
           
           txtid.getScene().setRoot(root);    }


    @FXML
    private void supid(ActionEvent event) {
     ServiceTest srv = new ServiceTest() ; 
     srv.suppr(Integer.parseInt(txtid.getText()));
     try {
             ServiceTest srv1 = new ServiceTest();
             ArrayList<Test> r = srv1.afiiche() ;
             ObservableList<Test>  list = FXCollections.observableArrayList();
             for (int i = 0; i < r.size(); i++)
             {
                 Test t = new Test();
                 t.setId(r.get(i).getId_f());
                 t.setQes(r.get(i).getQes());
                 t.setReponse(r.get(i).getReponse());
                 
                 list.add(t);
             }
             
             
             
             id.setCellValueFactory(new PropertyValueFactory<Test,Integer>("id"));
             qes.setCellValueFactory(new PropertyValueFactory<Test,String>("qes"));
             Reponse.setCellValueFactory(new PropertyValueFactory<Test,String>("Reponse"));
             
             table.setItems(list);
         } catch (SQLException ex) {
             Logger.getLogger(TestAdminController.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    
}
