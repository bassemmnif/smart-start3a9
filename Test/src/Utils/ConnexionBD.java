package Utils;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
public class ConnexionBD {
     private static    String url = "jdbc:mysql://127.0.0.1:3306/projet";
     private static String user="root";
     private static String password ="";
     private  Connection conn ; 
     private static  ConnexionBD inst ;
 public Connection getConn() {
        return conn;
    }
 public ConnexionBD() {
          try {
             conn = DriverManager.getConnection(url, user, password);
         } catch (SQLException ex) {
             Logger.getLogger(ConnexionBD.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    public  static ConnexionBD getInstance(){
    if (inst== null)
    { inst = new ConnexionBD (); }
    return inst ; 
    }
    
}
