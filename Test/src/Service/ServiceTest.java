/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Entities.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import Utils.ConnexionBD;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Asus
 */
public class ServiceTest {

    Connection c = ConnexionBD.getInstance().getConn();

    public void ajouterTest(Test t) {
        try {
            Statement st = c.createStatement();
            String req = "insert into test(id_f,qes,reponse) values('" + t.getId_f()+ "','" + t.getQes() + "','" + t.getReponse() + "')";
            st.execute(req);
        } catch (SQLException ex) {
            Logger.getLogger(ServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void Modifierqes(int id, String qes) throws SQLException {
        PreparedStatement pt = c.prepareStatement("update test set qes=? where id=? ");
        pt.setString(1, qes);
        pt.setInt(2, id);
        pt.executeUpdate();

    }

    public void Modifierrep(int id, String rep) throws SQLException {
        PreparedStatement pt = c.prepareStatement("update test set reponse=? where id=? ");
        pt.setString(1, rep);
        pt.setInt(2, id);
        pt.executeUpdate();

    }

    public void suppr(int id ) {
        try {
            PreparedStatement pt = c.prepareStatement("delete from test where id='"+id+"'");
           
            pt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int nbreC() {
        int i = 0;
        try {
            Statement st = c.createStatement();
            String req = "select * from test";
            ResultSet rs = st.executeQuery(req);
            while (rs.next()) {
                i = i + 1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceTest.class.getName()).log(Level.SEVERE, null, ex);

        }
        return i;
    }

    public ArrayList<Test> afiiche() throws SQLException {
        ArrayList<Test> myList = new ArrayList();
        Statement st = c.createStatement();
        String req = "select * from test";
        ResultSet rs = st.executeQuery(req);
        while (rs.next()) {
            Test t;

            t = new Test(rs.getInt(1), rs.getString(3), rs.getString(4));
            
            myList.add(t);

        }
        return myList;
    }
   public ArrayList<String> listf() throws SQLException 
   {
   ArrayList<String> myList = new ArrayList();
   Statement st = c.createStatement();
        String req = "select * from formation";
        ResultSet rs = st.executeQuery(req);
        while (rs.next())
        {
            myList.add(rs.getString(2));
        }
   return myList ;
   }
    public int idf (String nom)
       {
        int r= -1 ;
           try {
              Statement st = c.createStatement();
              String req = "SELECT id FROM formation where Nom='"+nom+"'";
              PreparedStatement pt = c.prepareStatement(req);
          
              ResultSet rs = st.executeQuery(req);
              while (rs.next())
              {  r = rs.getInt(1);
                      }
          } catch (SQLException ex) {
              Logger.getLogger(ServiceTest.class.getName()).log(Level.SEVERE, null, ex);
          }
          return r ; 
       }
 public String nomf (int id)
       {
       String r =""  ;
           try {
              Statement st = c.createStatement();
              String req = "SELECT * FROM formation where id='"+id+"'";
              PreparedStatement pt = c.prepareStatement(req);
          
              ResultSet rs = st.executeQuery(req);
              while (rs.next())
              {  r = rs.getString(2);
                      }
          } catch (SQLException ex) {
              Logger.getLogger(ServiceTest.class.getName()).log(Level.SEVERE, null, ex);
          }
          return r ; 
       }
 public ArrayList<String> listq(int id) throws SQLException
 {
 ArrayList<String> myList = new ArrayList();
   Statement st = c.createStatement();
        String req = "select * from test where id_f='"+id+"'";
        ResultSet rs = st.executeQuery(req);
    while (rs.next())
        {
            myList.add(rs.getString(3));
        }
   return myList ;
 }
 public ArrayList<String> listr(int id) throws SQLException
 {
 ArrayList<String> myList = new ArrayList();
   Statement st = c.createStatement();
        String req = "select * from test where id_f='"+id+"'";
        ResultSet rs = st.executeQuery(req);
    while (rs.next())
        {
            myList.add(rs.getString(4));
        }
   return myList ;
 }
}
