/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author Asus
 */
public class Test {
int    id,id_f,id_user;
String qes,reponse,nom_Formation;

    public String getNom_Formation() {
        return nom_Formation;
    }

    public void setNom_Formation(String nom_Formation) {
        this.nom_Formation = nom_Formation;
    }

    public Test() {
    }

    public Test(int id_f, String qes, String reponse) {
        this.id_f = id_f;
        this.qes = qes;
        this.reponse = reponse;
    }

    

    public Test( int id_f, int id_user, String qes, String reponse) {
       
        this.id_f = id_f;
        this.id_user = id_user;
        this.qes = qes;
        this.reponse = reponse;
    }

    public int getId() {
        return id;
    }

    public int getId_f() {
        return id_f;
    }

    public int getId_user() {
        return id_user;
    }

    public String getQes() {
        return qes;
    }

    public String getReponse() {
        return reponse;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setId_f(int id_f) {
        this.id_f = id_f;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public void setQes(String qes) {
        this.qes = qes;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    @Override
    public String toString() {
        return "Test{" + "id=" + id + ", id_f=" + id_f + ", id_user=" + id_user + ", qes=" + qes + ", reponse=" + reponse + '}';
    }
    

}
